import PropTypes from 'prop-types';

export const GifItem = ({ title, url }) => {
    return (
        <>
            <img
                src={url}
                alt={title} />
            <p>{title}</p>
        </>
    )
}

GifItem.propTypes = {
    title: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    id: PropTypes.string,
}