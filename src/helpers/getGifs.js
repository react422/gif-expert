export const getGifs = async (category) => {
    const url = `https://api.giphy.com/v1/gifs/search?q=${category}&limit=10&api_key=PlWDtfT2YD09AY9EHc6u02Ym8v33cxuL`;

    const resp = await fetch(url);
    const { data } = await resp.json();
    const gifs = data.map(img => (
        {
            id: img.id,
            title: img.title,
            url: img.images.downsized_medium.url, 
        }
    ))
    return gifs;
}
