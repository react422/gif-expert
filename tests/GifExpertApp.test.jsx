import { render, screen } from "@testing-library/react"
import { GifExpertApp } from "../src/GifExpertApp"


describe('Test on <GifExpertApp />', () => {

    test('should show loading message first', () => {
        render(<GifExpertApp />);
        // screen.debug();
        expect(screen.getByText('Loading...')).toBeTruthy();
    })

})