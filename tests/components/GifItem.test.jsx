import { render, screen } from "@testing-library/react"
import { GifItem } from "../../src/components"

describe('Test of <GifItem />', () => {

    const title = 'google';
    const url = 'https://google.com/google.jpg';

    test('should match the snapshot', () => {
        const { container } = render(<GifItem title={title} url={url} />)
        expect(container).toMatchSnapshot();
    })

    test('should show the image with the correct URL and ALT', () => {
        render(<GifItem title={title} url={url} />);
        // screen.debug(); 
        // expect(screen.getByRole('img').src).toBe(url);
        // expect(screen.getByRole('img').alt).toBe(title);
        const { src, alt } = screen.getByRole('img');
        expect(src).toBe(url);
        expect(alt).toBe(title);
    })

    test('should show the title of the component', () => {
        render(<GifItem title={title} url={url} />);
        expect(screen.getByText(title)).toBeTruthy();
    })
})