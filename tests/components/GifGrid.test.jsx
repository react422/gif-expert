import { render, screen } from "@testing-library/react";
import { GifGrid } from "../../src/components/GifGrid";
import { useFetchGifs } from "../../src/hook/useFetchGifs";

jest.mock('../../src/hook/useFetchGifs');

describe('Test <GifGrid />', () => {

    const category = 'One Punch';
    test('should show loading first', () => {

        useFetchGifs.mockReturnValue({
            images: [],
            isLoading: true
        });

        render(<GifGrid category={category} />);
        // screen.debug();
        expect(screen.getByText('Loading...'));
        expect(screen.getByText(category));
    })

    test('should show items with images are loaded', () => {
        // usually with fake data we have a dir called 'fixture'
        const gifs = [
            {
                id: "asdf34",
                title: "Saitama",
                url: "https://localhost/saitama.jpg"
            },
            {
                id: "poi1234",
                title: "Goku",
                url: "https://localhost/goku.jpg"
            },
        ];
        useFetchGifs.mockReturnValue({
            images: gifs,
            isLoading: false
        });
        render(<GifGrid category={category} />);

        expect(screen.getAllByRole('img').length).toBe(2);
        // screen.debug();
    })
})