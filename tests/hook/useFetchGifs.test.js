import { renderHook, waitFor } from "@testing-library/react";
import { useFetchGifs } from "../../src/hook/useFetchGifs"

describe('Test on useFetchGifs hook', () => {

    test('should return initial state', () => {

        const { result } = renderHook(() => useFetchGifs('one punch'));
        const { images, isLoading } = result.current;
        expect(images.length).toBe(0);
        expect(isLoading).toBeTruthy();
    })

    test('should return an array of images and isLoading must be false', async () => {

        const { result } = renderHook(() => useFetchGifs('one punch'));
        await waitFor(
            () => expect(result.current.images.length).toBeGreaterThan(0)
        );

        const { images, isLoading } = result.current;
        expect(images.length).toBeGreaterThan(0);
        expect(isLoading).toBeFalsy();
    })
})