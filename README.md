# gif-expert


## Create runner

```
docker volume create gitlab-runner-config

docker run -d --name runner-docker --restart always `
    -v /var/run/docker.sock:/var/run/docker.sock `
    -v gitlab-runner-config:/etc/gitlab-runner `
    gitlab/gitlab-runner:latest

docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner `
     gitlab/gitlab-runner:latest register

```

```
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "PROJECT_REGISTRATION_TOKEN" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "docker-runner" \
  --maintenance-note "Free-form maintainer notes about this runner" \
  --tag-list "docker,aws" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"
```